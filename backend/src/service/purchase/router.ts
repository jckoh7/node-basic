import express from "express";
import { config } from "../../middleware/config";
// import validate from 'express-validation';
import controll from "./controller"
// import validation from "./validation"
const expressJwt = require('express-jwt');

const router = express.Router();


/**
 * @swagger
 * definitions:
 *   PurchaseInfo:
 *     type: 'object'
 *     properties: 
 *       _id:
 *         type: 'string'
 *       wine_id:
 *         type: 'string'
 *       store_id:
 *         type: 'string'
 *       user_id:
 *         type: 'string'
 *       price:
 *         type: 'number'
 *       label:
 *         type: 'string'
 *       receipt:
 *         type: 'string'
 *       comment:
 *         type: 'string'
 *       c_dt:
 *         type: 'number'
 *       m_dt:
 *         type: 'number'
 *       c_id:
 *         type: 'string'
 *       m_id:
 *         type: 'string'
*/

/**
 * @swagger
 * paths:
 *   /api/v1/purchase/read/{id}:
 *     get:
 *       summary: 구매정보 단건 조회
 *       tags: [Purchase]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           description: '구매자 아이디'
 *       responses:
 *         200:
 *           description: 정보 조회 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 $ref: '#/definitions/PurchaseInfo'
 */ 
 router.route('/read/:id').get(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.getPurchaseById);

 /**
  * @openapi
  * paths:
  *   /api/v1/purchase/write:
  *     post:
  *       summary: 구매정보 저장
  *       tags: [Purchase]
  *       requestBody:
  *         content:
  *           application/json: 
  *             name: Purchases Info
  *             description: '구매 정보'
  *             schema:
  *               $ref: '#/definitions/Purchase'
  *             examples:
  *               Sample1:
  *                 value:
  *                   wine_id: '123123123'
  *                   store_id: '12312321'
  *                   user_id: '1231231'
  *                   price: 10000
  *                   label: 'label'
  *       responses:
  *         200:
  *           description: 구매정보 저장 성공
  *           schema:
  *             type: object
  *             properties:
  *               status:
  *                 type: boolean
  *               message:
  *                 type: string
  *               data:
  *                 type: object
  *                 properties:
  *                   _id: 
  *                     type: string
  */ 
 router.route('/write').post(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.writePurchase);
 
 /**
  * @openapi
  * paths:
  *   /api/v1/purchase/delete:
  *     delete:
  *       summary: 구매정보 삭제 
  *       tags: [Purchase]
  *       requestBody:
  *         content:
  *           application/json: 
  *             name: purchase info
  *             description: 'wine object'
  *             schema:
  *               type: array
  *               items:
  *                 type: object
  *                 properties:
  *                   _id:
  *                     type: string
  *       responses:
  *         200:
  *           description: 구매정보 삭제 성공
  *           schema:
  *             type: object
  *             properties:
  *               status:
  *                 type: boolean
  *               message:
  *                 type: string
  *               data:
  *                 type: array
  *                 items:
  *                   type: object
  *                   properties:
  *                     _id: 
  *                       type: string
  */ 
  router.route('/delete').delete(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.deletePurchase);

  export default router;