import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthRoutingModule } from './module/auth/auth-routing.module';
import { HomeRoutingModule } from './module/home/home-routing.module';
import { NotFoundComponent } from './shared/container/not-found/not-found.component';
import { SharedRoutingModule } from './shared/shared-routing.module';
const routes: Routes = [
  { path: '', redirectTo: 'auth/signin', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    SharedRoutingModule,
    AuthRoutingModule,
    HomeRoutingModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
