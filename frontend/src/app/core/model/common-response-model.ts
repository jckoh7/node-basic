
import { Injectable } from '@angular/core';
import has = Reflect.has;

export interface CommonModel {
  resultCode: string;
  resultMessage: string;
  detailMessage: string;
  data?: any;
  list?: any[];
}

export class CommonResponse implements CommonModel {
  resultCode: string;
  resultMessage: string;
  detailMessage: string;
  data?: any;
  list?: any[];

  constructor(props: CommonModel);
  constructor(props?: any) {
  // constructor(resultCode: string, resultMessage: string, detailMessage: string, response: Response) {
    this.resultCode = props?.resultCode || 'E000';
    this.resultMessage = props?.resultMessage || 'Unknow Error';
    this.detailMessage = props?.detailMessage || '';
    this.data = props?.data;
    this.list = props?.list;
  }
}


export interface BaseAdapter {
  adapt(item: any): any;
}

@Injectable({
  providedIn: 'root'
})
export class CommonResponseAdapter implements BaseAdapter {
  adapt(item: any): CommonResponse | any {
    if (item instanceof Response) {
      item = item as Response;
      return new CommonResponse({
        resultCode: item.status === 200 ? 'S000' : 'E000',
        resultMessage: item.status !== 200 ? item.statusText : '',
        detailMessage: ''
      });
    } else if (has(item, 'resultCode')){
      return new CommonResponse({
        resultCode: item?.resultCode || 'E000',
        resultMessage: item?.resultMessage || 'Unknow Error',
        detailMessage: item?.detailMessage || '',
        data: item?.data,
        list: item?.list
      });
    } else {
      return item;
    }
  }
}
