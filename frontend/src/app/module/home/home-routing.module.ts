import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedGuard } from 'src/app/shared/shared.guard';
import { IndexComponent } from './container/index/index.component';
import { MainComponent } from './container/main/main.component';
import { RegTradeComponent } from './container/reg-trade/reg-trade.component';
import { SearchComponent } from './container/search/search.component';

const routes: Routes = [
    { path: 'home', component: IndexComponent },
    { path: 'home/main', component: MainComponent, canActivate: [SharedGuard] },
    { path: 'home/search', component: SearchComponent, canActivate: [SharedGuard] },
    { path: 'home/reg-trade', component: RegTradeComponent, canActivate: [SharedGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
      SharedGuard,
  ]
})
export class HomeRoutingModule { }
