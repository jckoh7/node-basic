import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { CoreState } from 'src/app/core/core.state';

@Injectable()
export class HomeState extends CoreState {
  
  constructor() {
      super();
  }
}
