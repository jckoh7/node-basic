import express from "express";
import { config } from "../../middleware/config";
// import validate from 'express-validation';
import controll from "./controller"
// import validation from "./validation"
const expressJwt = require('express-jwt');

const router = express.Router();


/**
 * @swagger
 * definitions:
 *   Review:
 *     type: 'object'
 *     properties: 
 *       _id:
 *         type: 'string'
 *       wine_id:
 *         type: 'string'
 *       user_id:
 *         type: 'string'
 *       acid:
 *         type: 'number'
 *       tannin:
 *         type: 'number'
 *       bodied:
 *         type: 'number'
 *       sweetness:
 *         type: 'number'
 *       alcohol:
 *         type: 'number'
 *       comment:
 *         type: 'string'
 *       c_dt:
 *         type: 'number'
 *       m_dt:
 *         type: 'number'
 *       c_id:
 *         type: 'string'
 *       m_id:
 *         type: 'string'
 */

/**
 * @swagger
 * paths:
 *   /api/v1/review/read/{id}:
 *     get:
 *       summary: 리뷰 단건 조회
 *       tags: [Review]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           description: '작성자 아이디'
 *       responses:
 *         200:
 *           description: 리뷰 조회 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 $ref: '#/definitions/Review'
 */ 
 router.route('/read/:id').get(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.getReviewById);

 /**
  * @openapi
  * paths:
  *   /api/v1/review/write:
  *     post:
  *       summary: 리뷰 저장
  *       tags: [Review]
  *       requestBody:
  *         content:
  *           application/json: 
  *             name: Purchases Info
  *             description: '리뷰 내용'
  *             schema:
  *               $ref: '#/definitions/Review'
  *             examples:
  *               SampleWine1:
  *                 value:
  *                   wineId: '123123123'
  *                   acid: 3
  *                   tannin: 2
  *                   bodied: 4
  *                   sweetness: 1
  *                   alcohol: 4
  *                   comment: 'good~~'
  *       responses:
  *         200:
  *           description: 리뷰 저장 성공
  *           schema:
  *             type: object
  *             properties:
  *               status:
  *                 type: boolean
  *               message:
  *                 type: string
  *               data:
  *                 type: object
  *                 properties:
  *                   _id: 
  *                     type: string
  */ 
 router.route('/write').post(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.writeReview);
 
 /**
  * @openapi
  * paths:
  *   /api/v1/review/delete:
  *     delete:
  *       summary: 리뷰 삭제 
  *       tags: [Review]
  *       requestBody:
  *         content:
  *           application/json: 
  *             name: review 
  *             description: 'review object'
  *             schema:
  *               type: array
  *               items:
  *                 type: object
  *                 properties:
  *                   _id:
  *                     type: string
  *       responses:
  *         200:
  *           description: 리뷰 삭제 성공
  *           schema:
  *             type: object
  *             properties:
  *               status:
  *                 type: boolean
  *               message:
  *                 type: string
  *               data:
  *                 type: array
  *                 items:
  *                   type: object
  *                   properties:
  *                     _id: 
  *                       type: string
  */ 
  router.route('/delete').delete(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.deleteReview);

  export default router;