import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import { AuthFacade } from '../module/auth/auth.facade';

@Injectable()
export class SharedGuard implements CanActivate {

  constructor(
    private router: Router,
    private facade: AuthFacade,
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // if (environment.production && !util.isWebview()) {
    //   setTimeout(() => {
    //     window.location.href = 'https://www.macaront.com';
    //   }, 700);
    //   return false;
    // }

    if (this.facade.getToken() != null) {
        return true;
      }
      const token = next.queryParams['token'];
      if (token != null) {
        this.facade.setToken(token);
        return true;
      }
      // 로그인 하지 않은 경우, 자동으로 signin 컴포넌트로 이동
      this.router.navigate(['/auth/signin']);
      return false;
    }
}
