import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './container/main/main.component';
import { SearchComponent } from './container/search/search.component';
import { RegTradeComponent } from './container/reg-trade/reg-trade.component';
import { AddWineComponent } from './component/add-wine/add-wine.component';
import { AddStoreComponent } from './component/add-store/add-store.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SharedGuard } from 'src/app/shared/shared.guard';
import { IndexComponent } from './container/index/index.component';



@NgModule({
  declarations: [
    MainComponent, 
    SearchComponent, 
    RegTradeComponent, 
    AddWineComponent, 
    AddStoreComponent, 
    IndexComponent
  ],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
  ],
  providers: [
  ]
})
export class HomeModule { 
  static forRoot(): any {
    return {
      ngModule: HomeModule,
      providers: [
      ]
    };
  }
}
