
import MongoDBProvider from "../../provider/mongo.database.provider";

export interface IPhoto {
    _id: string;
    gphoto_id: string;
    rawUrl: string;
    width: number;
    height: number;
    c_id: string;
    m_id: string;
    c_dt: number;
    m_dt: number;
}

function bindCollection<T>(db: MongoDBProvider) { 
    return {
        async getAll(page = 0, size = 20, query) {
            let result = await db.find<T>(page, size, query);
            return result;
        },
        
        async getById(id) {
            let result = await db.find<T>(0, 20, {"_id": id});
            return result.length > 0 ? result[0] : {};
        },

        async create(wine) {
            return await db.insertOne<T>(wine)
        },

        async update(id, wine) {
            return await db.findOneAndUpdate<T>({"_id": id}, {$set: wine}, { returnNewDocument: true });
        },

        async updateWithPipe(id, pipe) {
            return await db.findOneAndUpdate<T>({"_id": id}, pipe, { returnNewDocument: true });
        },

        async delete(id) {
            return await db.deleteOne({_id: id});
        }
    };
}

const photo = bindCollection<IPhoto>(new MongoDBProvider('WineQuery/Photo'));

export default {
    photo
};