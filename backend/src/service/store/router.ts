import express from "express";
import { config } from "../../middleware/config";
// import validate from 'express-validation';
import controll from "./controller"
// import validation from "./validation"
const expressJwt = require('express-jwt');

const router = express.Router();


/**
 * @swagger
 * definitions:
 *   Store:
 *     type: 'object'
 *     properties: 
 *       _id:
 *         type: 'string'
 *       name:
 *         type: 'string'
 *       addr:
 *         type: 'string'
 *       lat:
 *         type: 'number'
 *       lng:
 *         type: 'number'
 *       start:
 *         type: 'string'
 *       finish:
 *         type: 'string'
 *       closed:
 *         type: 'string[]'
 *       sales_cnt:
 *         type: 'number'
 *       review_point:
 *         type: 'number'
 *       c_dt:
 *         type: 'number'
 *       m_dt:
 *         type: 'number'
 *       c_id:
 *         type: 'string'
 *       m_id:
 *         type: 'string'
 */

/**
 * @openapi
 * paths:
 *   /api/v1/store/search:
 *     get:
 *       summary: 상점 조회 - GPS 기반
 *       tags: [Store]
 *       parameters:
 *         - in: query
 *           name: lat
 *           schema:
 *             type: number
 *           description: 'Latitude'
 *         - in: query
 *           name: lng
 *           schema:
 *             type: number
 *           description: 'Longitude'
 *         - in: query
 *           name: page
 *           schema:
 *             type: integer
 *           description: '페이지 번호 - 기본 0'
 *         - in: query
 *           name: size
 *           schema:
 *             type: integer
 *           description: '페이지 사이즈 - 기본 20'
 *       responses:
 *         200:
 *           description: 상점 조회 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 type: array
 *                 items:
 *                   $ref: '#/definitions/Store'
 */ 
 router.route('/search').get(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.searchFromGPS);

/**
 * @swagger
 * paths:
 *   /api/v1/store/read/{id}:
 *     get:
 *       summary: 상점 단건 조회
 *       tags: [Store]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           description: '작성자 아이디'
 *       responses:
 *         200:
 *           description: 상점 조회 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 $ref: '#/definitions/Store'
 */ 
 router.route('/read/:id').get(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.getStoreById);

 /**
  * @openapi
  * paths:
  *   /api/v1/store/write:
  *     post:
  *       summary: 상점 저장
  *       tags: [Store]
  *       requestBody:
  *         content:
  *           application/json: 
  *             name: Purchases Info
  *             description: '상점 내용'
  *             schema:
  *               $ref: '#/definitions/Store'
  *             examples:
  *               SampleWine1:
  *                 value:
  *                   name: '스카이 샵'
  *                   addr: '서울시 중구'
  *                   start: '09:00'
  *                   finish: '20:00'
  *                   closed: ['sun','sat']
  *                   lat: 37.563032,
  *                   lng: 126.981425
  *       responses:
  *         200:
  *           description: 상점 저장 성공
  *           schema:
  *             type: object
  *             properties:
  *               status:
  *                 type: boolean
  *               message:
  *                 type: string
  *               data:
  *                 type: object
  *                 properties:
  *                   _id: 
  *                     type: string
  */ 
 router.route('/write').post(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.writeStore);
 
 /**
  * @openapi
  * paths:
  *   /api/v1/store/delete:
  *     delete:
  *       summary: 상점 삭제 
  *       tags: [Store]
  *       requestBody:
  *         content:
  *           application/json: 
  *             name: store 
  *             description: 'store object'
  *             schema:
  *               type: array
  *               items:
  *                 type: object
  *                 properties:
  *                   _id:
  *                     type: string
  *       responses:
  *         200:
  *           description: 상점 삭제 성공
  *           schema:
  *             type: object
  *             properties:
  *               status:
  *                 type: boolean
  *               message:
  *                 type: string
  *               data:
  *                 type: array
  *                 items:
  *                   type: object
  *                   properties:
  *                     _id: 
  *                       type: string
  */ 
  router.route('/delete').delete(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.deleteStore);

  export default router;