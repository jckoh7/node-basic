import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './container/signin/signin.component';
import { SignupComponent } from './container/signup/signup.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthApi } from './auth.api';
import { AuthFacade } from './auth.facade';
import { AuthGuard } from './auth.guard';


@NgModule({
  declarations: [
  SigninComponent,
  SignupComponent],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
  ],
  providers: [
    AuthApi,
    AuthFacade,
    AuthGuard,
  ]
})
export class AuthModule {
  static forRoot(): any {
    return {
      ngModule: AuthModule,
      providers: [
        AuthApi,
        AuthFacade,
        AuthGuard,
      ]
    };
  }
}
