declare global {
    interface String {
      string(len: number): string;
      zf(len: number): string;
    }
    interface Number {
      zf(len: number): string;
    }
  }
  
  String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
  String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
  Number.prototype.zf = function(len){return this.toString().zf(len);};
  
  import * as device from 'mobile-device-detect';
  import jwt_decode from 'jwt-decode';
  
  export default {
    replacePhoneNum(phoneNum: string): string {
      const numberRegExp = phoneNum.replace(/[^0-9]/g, '');
      const digits = numberRegExp.replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/, '$1-$2-$3').replace('--', '-');
      return digits;
    },
    dateToStringWithFormat(date, format): string {
      if (!date || !format) {return; }
  
      var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
      var d = date;
      var h = 0;
  
      return format.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
          case "yyyy":
            return d.getFullYear();
          case "yy":
            return (d.getFullYear() % 1000).zf(2);
          case "MM":
            return (d.getMonth() + 1).zf(2);
          case "dd":
            return d.getDate().zf(2);
          case "E":
            return weekName[d.getDay()];
          case "HH":
            return d.getHours().zf(2);
          case "hh":
            return ((h = d.getHours() % 12) ? h : 12).zf(2);
          case "mm":
            return d.getMinutes().zf(2);
          case "ss":
            return d.getSeconds().zf(2);
          case "ddd":
            return d.getHours() < 12 ? "오전" : "오후";
          default:
            return $1;
        }
      });
    },
    getNumberWithComma(num): string {
      if (num) {
        return `${num}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
      return '0';
    },
    readableTime(sec): string {
      let ss = sec < 60 ? 60 : sec;
      var mm = Math.floor(ss / 60);
      var hh = Math.floor(mm / 60);
      var dd = Math.floor(hh / 24);
      mm = mm % 60;
      hh = hh % 24;
  
      if (dd > 0) return dd + '일 ';
      if (hh > 0) return `${hh ? hh + '시간 ' : ''}${mm ? mm + '분 ' : ''}`;
      return mm + '분 ';
    },
    getOS(): any {
      var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;
  
      if (macosPlatforms.indexOf(platform) !== -1) {
        os = 'Mac OS';
      } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = 'iOS';
      } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = 'Windows';
      } else if (/Android/.test(userAgent)) {
        os = 'Android';
      } else if (!os && /Linux/.test(platform)) {
        os = 'Linux';
      }
      return os;
    },
    isMobile(): any {
      const os = this.getOS();
      return (os === 'iOS' || os === 'Android');
    },
    isWebview(): boolean {
      var webview = ['WebKit', 'Chrome WebView', 'WebView'];
      webview = webview.filter((n) => n === device.browserName);
      return (Array.isArray(webview) && webview.length > 0);
    },
    goSchemeUrl(url, fallback): any {
      // Solv. 1
      // var script = document.createElement('script');
      //
      // script.onload = function() {
      //     document.location = url;
      //     el.remove();
      // }
      // script.onerror = function() {
      //     window.location.href = fallback;
      //     el.remove();
      // }
      // script.setAttribute('src', url);
      //
      // const el = document.getElementsByTagName('head')[0].appendChild(script);
  
  
      // Solv. 2
      // var xmlhttp=new XMLHttpRequest();
      // xmlhttp.open('GET', url, false);
      // try {
      //   xmlhttp.send(null);                  // Send the request now
      // } catch (e) {
      //   document.location = fallback;
      //   return;
      // }
      //
      // // Throw an error if the request was not 200 OK
      // if (xmlhttp.status === 200) {
      //   document.location = url;
      // } else {
      //   document.location = fallback;
      // }
  
      // Solv. 3
      if (this.isWebview()) {
        // 마카롱 앱의 경우 Javascript 인터페이스를 제공 하므로 인터페이스 이름(macaron)으로 마카롱 앱인지 여부를 판단한다.
        if (window['macaron'] != undefined ) {
          // 마카롱 앱인경우 스킴 호출.
          window.location.href = url;
        } else {
          window.location.href = fallback;
        }
      } else {
        window.location.href = fallback;
      }
    },
    getDecodedJWT(token: string, forKey: string): any {
      try{
        const payload = jwt_decode(token);
        if (forKey != null) {
          return payload[forKey];
        } else {
          return payload;
        }
      }
      catch(Error){
        return null;
      }
    },
  }
  