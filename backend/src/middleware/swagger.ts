'use strict';

const swaggerJSDoc = require('swagger-jsdoc');

const swaggerOption = {
    swaggerDefinition: {
        openapi: "3.0.1",
        // 정보
        info: {
            title: 'Node.js + Express + MongoDB',
            version: '1.0.0',
            description: 'Node.js Express API Server.'
        },
        contact: {
            email: "kstmdev@gmail.com"
        },
        // 각 api에서 설명을 기록할 때 사용할 constant들을 미리 등록해놓는것
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            },
            res: {
                BadRequest: {
                    description: '잘못된 요청.',
                    schema: {
                        $ref: '#/components/errorResult/Error'
                    }
                },
                Forbidden: {
                    description: '권한이 없슴.',
                    schema: {
                        $ref: '#/components/errorResult/Error'
                    }
                },
                NotFound: {
                    description: '없는 리소스 요청.',
                    schema: {
                        $ref: '#/components/errorResult/Error'
                    }
                }
            },
            errorResult: {
                Error: {
                    type: 'object',
                    properties: {
                        resultCode: 'integer',
                        resultMessage: {
                            type: 'string',
                            description: '에러 메시지 전달.'
                        }
                    }
                }
            }
        },
        security: [{
            bearerAuth: []
        }],
    },
    apis: [
        './**/router.ts'
    ], // api 파일 위치들
    securityDefinitions: {
        Bearer: {
            type: 'api_key',
            name: 'Authorization',
            in: "header"
        }
    },
};

export const swaggerSpec = swaggerJSDoc(swaggerOption);


