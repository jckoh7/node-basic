import IAuthProvider from "./auth.provider";

const firebase = require('firebase');

export default class FirebaseAuthProvider implements IAuthProvider {
    signin(q: any): Promise<any> {
        let {email, password} = q;
        if (!email || !password) {
            throw Error('Bad Request.');
        }
        return new Promise( (resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then((userCredential) => {
                // Signed in
                var user = userCredential.user;
                resolve(user);
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                reject({status:false, message: errorMessage, data: errorCode});
            });
        });
    }
    signup(q: any): Promise<any> {
        let {email, password} = q;
        if (!email || !password) {
            throw Error('Bad Request.');
        }
        return new Promise( (resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((userCredential) => {
                // Signed in
                var user = userCredential.user;
                resolve(user);

            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                reject({status:false, message: errorMessage, data: errorCode});
            });
        });
    }
    signout(q: any): Promise<any> {
        return new Promise( (resolve, reject) => {
            reject({status: false, message: 'Method not implemented.'});
        });
    }
    auth(q: any): Promise<any> {
        return new Promise( (resolve, reject) => {
            reject({status: false, message: 'Method not implemented.'});
        });
    }
    confirm(q: any): Promise<any> {
        return new Promise( (resolve, reject) => {
            reject({status: false, message: 'Method not implemented.'});
        });
    }

    onAuthStatus(callback:(info: any) => void): void {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                // User is signed in, see docs for a list of available properties
                // https://firebase.google.com/docs/reference/js/firebase.User
                var uid = user.uid;
                callback(user);
            } else {
                callback('anonymouse');
            }
        });        
    }

}