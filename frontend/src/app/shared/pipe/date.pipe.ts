import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  name:'customdate',
})
export class CustomDatePipe extends DatePipe {

  transform(value: any, ...args): any {
    if (!value?.includes('T') && value?.includes(' ')) {
      value = value.replace(' ', 'T');
    }
    return super.transform(value, ...args);
  }
}
